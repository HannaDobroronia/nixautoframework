package NixNavigationSpec

import Pages.BlogPage
import Pages.StartPage
import geb.spock.GebReportingSpec

class NavigatetoBlogpage extends GebReportingSpec{
    def "Navigate to Blog page"() {
        when:
        to StartPage
        and:
        "User navigates to Blog page"()
        then:
        at BlogPage
    }

}
